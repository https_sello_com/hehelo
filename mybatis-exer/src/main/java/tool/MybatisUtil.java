package tool;

import java.io.IOException;
import java.io.InputStream;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

public class MybatisUtil {

	static SqlSessionFactory sessionFactory;

	static {

		// 1.创建SqlSessionFactoryBuilder
		String resource = "mybatis-config.xml";

		SqlSessionFactoryBuilder builder = new SqlSessionFactoryBuilder();

		// 2.创建SqlSessionFactory
		InputStream is = null;
		try {
			is = Resources.getResourceAsStream(resource);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		sessionFactory = builder.build(is);
	}
	
	public static SqlSessionFactory findSqlSessionFactory(){
		
		return sessionFactory;
	}

	
	
	
	
	public static void main(String[] args) throws IOException {

		// 1.创建SqlSessionFactoryBuilder
		String resource = "mybatis-config.xml";

		SqlSessionFactoryBuilder builder = new SqlSessionFactoryBuilder();

		// 2.创建SqlSessionFactory
		InputStream is = Resources.getResourceAsStream(resource);

		SqlSessionFactory sessionFactory = builder.build(is);

		// 3.创建SqlSession
		// session线程不安全，使用在方法中（处于方法作用域中）
		SqlSession session = sessionFactory.openSession();

		System.out.println(session);

		// 4.使用SqlSession进行CRUD操作

	}

}
