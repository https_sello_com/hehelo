package service;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import mapper.StudentMapper;
import pojo.Students;
import tool.MybatisUtil;

public class StudentService {

	private SqlSessionFactory ssf = MybatisUtil.findSqlSessionFactory();

	public void save(Students stu) {

		SqlSession session = ssf.openSession();
		try {
			StudentMapper mapper = session.getMapper(StudentMapper.class);
			mapper.insert(stu);
			session.commit();
		} catch (Exception e) {
			session.rollback();
			// TODO: handle exception
		} finally {
			session.close();
		}
	}

	public void remove(Integer id) {
		SqlSession session = ssf.openSession();
		StudentMapper mapper = session.getMapper(StudentMapper.class);
		mapper.delete(id);
		session.commit();
		session.close();
	}

	public void Update(Students stu) {

		SqlSession session = ssf.openSession();
		try {
			StudentMapper mapper = session.getMapper(StudentMapper.class);
			mapper.update(stu);
			session.commit();
		} catch (Exception e) {
			session.rollback();
			// TODO: handle exception
		} finally {
			session.close();
		}
	}
	
	public Students findStu(Integer id) {
		SqlSession session = ssf.openSession();
		Students stu=null;
		try {
			StudentMapper mapper = session.getMapper(StudentMapper.class);
			stu = mapper.selectbyId(12);
			session.commit();
		} catch (Exception e) {
			session.rollback();
			// TODO: handle exception
		} finally {
			session.close();
		}
		return stu;
	}

	public List<Students> findStu2() {
		SqlSession session = ssf.openSession();
		List<Students> list=null;
		try {
			StudentMapper mapper = session.getMapper(StudentMapper.class);
			list = mapper.selectAll();
			session.commit();
		} catch (Exception e) {
			session.rollback();
			// TODO: handle exception
		} finally {
			session.close();
		}
		return list;
	}
	
	public List<Map> findStu3() {
		SqlSession session = ssf.openSession();
		List<Map> map =null;
		try {
			StudentMapper mapper = session.getMapper(StudentMapper.class);
			map = mapper.selectListMap();
			session.commit();
		} catch (Exception e) {
			session.rollback();
			// TODO: handle exception
		} finally {
			session.close();
		}
		return map;
	}
}
